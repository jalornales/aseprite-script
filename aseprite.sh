#!/bin/bash

# first install dependencies

# for apt 
# sudo apt-get install -y g++ cmake ninja-build libx11-dev libxcursor-dev libxi-dev libgl1-mesa-dev libfontconfig1-dev aria2 

# for dnf 
# sudo dnf install -y gcc-c++ cmake ninja-build libX11-devel libXcursor-devel libXi-devel  mesa-libGL-devel fontconfig-devel aria2 

# pacman
# sudo pacman -S gcc cmake ninja libx11 libxcursor mesa-libgl fontconfig aria2


mkdir -p ~/.local/opt && cd ~/.local/opt/ || exit
# aria2c "https://github.com/aseprite/skia/releases/download/m96-2f1f21b8a9/Skia-Linux-Release-x64.zip" # this is the latest pre-built binary as of the time of writing this. visit for latest release 'https://github.com/aseprite/skia/releases'
# unzip Skia-Linux-Release-x64.zip -d ./skia

# or build skia yourself; taken from 'https://github.com/aseprite/skia#skia-on-linux'
git clone --depth=1 https://chromium.googlesource.com/chromium/tools/depot_tools.git
git clone --depth=1 https://github.com/aseprite/skia.git
export PATH="${PWD}/depot_tools:${PATH}"
cd skia || exit 
python tools/git-sync-deps
gn gen out/Release-x64 --args="is_debug=false is_official_build=true skia_use_system_expat=false skia_use_system_icu=false skia_use_system_libjpeg_turbo=false skia_use_system_libpng=false skia_use_system_libwebp=false skia_use_system_zlib=false skia_use_sfntly=false skia_use_freetype=true skia_use_harfbuzz=true skia_pdf_subset_harfbuzz=true skia_use_system_freetype2=false skia_use_system_harfbuzz=false"
ninja -C out/Release-x64 skia modules

git clone --depth=1 https://github.com/aseprite/aseprite ~/git/aseprite
cd ~/git/aseprite || exit
git submodule update --init --recursive --depth=1

mkdir -p build && cd build || exit
# don't know if WEBP still have issue but still added '-DENABLE_WEBP=off'
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DLAF_BACKEND=skia -DSKIA_DIR=$HOME/.local/opt/skia -DSKIA_LIBRARY_DIR=$HOME/.local/opt/skia/out/Release-x64 -DSKIA_LIBRARY=$HOME/.local/opt/skia/out/Release-x64/libskia.a -DENABLE_WEBP=off -G Ninja .. && cmake --build . --target aseprite
